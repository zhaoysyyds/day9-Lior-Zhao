package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final JPAEmployeeRepository jpaEmployeeRepository;
    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }


    public List<Employee> findAll() {
        return jpaEmployeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, Employee employee) {
        Employee findEmployee=jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        if (employee.getSalary() != null) {
            findEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            findEmployee.setAge(employee.getAge());
        }
        jpaEmployeeRepository.save(findEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return jpaEmployeeRepository.findByGender(gender);
    }

    public Employee create(Employee employee) {
        return jpaEmployeeRepository.save(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size){
        PageRequest pageRequest = PageRequest.of(page-1, size);
        Page<Employee> employeePage=jpaEmployeeRepository.findAll(pageRequest);
        return employeePage.getContent();
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
