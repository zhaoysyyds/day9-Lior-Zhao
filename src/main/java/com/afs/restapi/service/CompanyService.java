package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {


    private final JPACompanyRepository jpaCompanyRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JPACompanyRepository jpcRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaCompanyRepository = jpcRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }
    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        Page<Company> employeePage = jpaCompanyRepository.findAll(pageRequest);
        return employeePage.getContent();
    }

    public Company findById(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaCompanyRepository.findById(id).map(Company::getEmployees).orElseThrow(CompanyNotFoundException::new);
    }

    public void update(Long id, Company company) {
        Company findCompany = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        if (company.getName() != null) {
            findCompany.setName(company.getName());
        }
        jpaCompanyRepository.save(findCompany);
    }

    public Company create(Company company) {
        return jpaCompanyRepository.save(company);
    }


    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
